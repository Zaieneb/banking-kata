package com.zaieneb.bank.service;

import com.zaieneb.bank.entities.Account;
import com.zaieneb.bank.entities.Transaction;
import com.zaieneb.bank.exceptions.AccountExistException;
import com.zaieneb.bank.exceptions.NegatifAmountException;
import com.zaieneb.bank.exceptions.UnsufficientAmountException;
import com.zaieneb.bank.repository.AccountRepository;
import com.zaieneb.bank.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import javax.security.auth.login.AccountNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private final AccountRepository accountRepository;
    private final TransactionRepository transactionRepository;

    public AccountService(AccountRepository accountRepository,
                          TransactionRepository transactionRepository) {
        this.accountRepository = accountRepository;
        this.transactionRepository = transactionRepository;
    }

    public Account createAccount(Account account) throws AccountExistException{

        Optional<Account> acc=this.accountRepository.findById(account.getId());

        if(acc.isPresent()){
            throw new AccountExistException("This account already exists");
        }

   return  this.accountRepository.save(account);

    }

    public void deposit(BigDecimal deposit, Account account) throws NegatifAmountException, AccountNotFoundException {
        Account acc=this.accountRepository.findById(account.getId()).orElseThrow(() -> new AccountNotFoundException("Account do not exist"));
        if(deposit.compareTo(BigDecimal.ZERO) < 0 ){
            throw new NegatifAmountException("Transaction failed : Cannot deposit negative amounts. Please enter a different amount.");
        }

        acc.setAmount(acc.getAmount().add(deposit));

        accountRepository.save(acc);
        Transaction transaction = new Transaction(LocalDateTime.now(),deposit);
        transactionRepository.save(transaction);

    }

    public void withdrawl(BigDecimal withdrawl,Account account) throws UnsufficientAmountException, AccountNotFoundException {
        Account acc=this.accountRepository.findById(account.getId()).orElseThrow(() -> new AccountNotFoundException("Account do not exist"));
        if(withdrawl.compareTo(acc.getAmount()) > 0) {
            throw new UnsufficientAmountException("Transaction failed : Cannot withdrawl");
        }

        acc.setAmount(acc.getAmount().subtract(withdrawl));

        accountRepository.save(acc);
        Transaction transaction = new Transaction(LocalDateTime.now(),withdrawl.negate());
        transactionRepository.save(transaction);


    }

    public List<Transaction> getAccountHistory(Account account) throws AccountNotFoundException {
        Account acc=this.accountRepository.findById(account.getId()).orElseThrow(() -> new AccountNotFoundException("Account do not exist"));
        return transactionRepository.findAllByAccount(acc);
    }
}
