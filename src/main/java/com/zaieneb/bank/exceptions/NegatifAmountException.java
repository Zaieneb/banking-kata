package com.zaieneb.bank.exceptions;

public class NegatifAmountException extends Exception{
    public NegatifAmountException(String message) {
        super(message);
    }
}
