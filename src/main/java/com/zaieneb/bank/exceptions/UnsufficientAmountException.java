package com.zaieneb.bank.exceptions;

public class UnsufficientAmountException extends Exception{
    public UnsufficientAmountException(String message) {
        super(message);
    }
}
