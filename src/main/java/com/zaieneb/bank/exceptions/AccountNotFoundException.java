package com.zaieneb.bank.exceptions;

public class AccountNotFoundException extends Exception{
    AccountNotFoundException(String message){
        super(message);
    }
}
