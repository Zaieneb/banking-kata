package com.zaieneb.bank.exceptions;

public class AccountExistException extends Exception{
    public AccountExistException(String message){
        super(message);
    }
}
