package com.zaieneb.bank;


import com.zaieneb.bank.entities.Account;
import com.zaieneb.bank.exceptions.AccountExistException;
import com.zaieneb.bank.exceptions.NegatifAmountException;
import com.zaieneb.bank.exceptions.UnsufficientAmountException;
import com.zaieneb.bank.repository.AccountRepository;
import com.zaieneb.bank.repository.TransactionRepository;
import com.zaieneb.bank.service.AccountService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.security.auth.login.AccountNotFoundException;
import java.math.BigDecimal;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
@RunWith(SpringRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepositoryMock;

    @Mock
    private TransactionRepository transactionRepositoryMock;
    private AccountService accountService;

    @Before
    public void before(){
        accountService = new AccountService(accountRepositoryMock,transactionRepositoryMock);
    }

    @Test
    public void shall_fail_deposit() throws AccountNotFoundException, AccountExistException, NegatifAmountException {
        Account acc = new Account();
        acc.setAmount(new BigDecimal(200));
        acc.setId(new Random().nextLong());
        Account account= this.accountService.createAccount(acc);

        this.accountService.deposit(new BigDecimal(-20),account);

        assertEquals(account.getAmount(),new BigDecimal(200));

    }

    @Test
    public void shall_success_deposit() throws AccountNotFoundException, NegatifAmountException, AccountExistException {
        Account acc = new Account();
        acc.setAmount(new BigDecimal(200));
        acc.setId(new Random().nextLong());
        Account account =  this.accountService.createAccount(acc);

        this.accountService.deposit(new BigDecimal(20),account);

        assertEquals(acc.getAmount(),new BigDecimal(220));

    }

    @Test
    public void shall_fail_withdrawl() throws AccountNotFoundException, AccountExistException, UnsufficientAmountException {
        Account acc = new Account();
        acc.setAmount(new BigDecimal(200));
        acc.setId(new Random().nextLong());
        Account account= this.accountService.createAccount(acc);

        this.accountService.withdrawl(new BigDecimal(300),account);

        assertEquals(account.getAmount(),new BigDecimal(200));

    }

    @Test
    public void shall_success_withdrawl() throws AccountNotFoundException, AccountExistException, UnsufficientAmountException {
        Account acc = new Account();
        acc.setAmount(new BigDecimal(200));
        acc.setId(new Random().nextLong());
        Account account= this.accountService.createAccount(acc);

        this.accountService.withdrawl(new BigDecimal(50),account);

        assertEquals(account.getAmount(),new BigDecimal(150));

    }
}
